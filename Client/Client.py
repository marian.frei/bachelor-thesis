import struct
import socket
import pickle
import json
from torch.optim import SGD, Adam, AdamW
import sys
import time
import random
import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import matplotlib.pyplot as plt
#import seaborn as sns
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import Dataset, DataLoader
from torch.autograd import Variable
from sklearn.model_selection import train_test_split
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.metrics import accuracy_score, auc, f1_score, precision_score, recall_score, roc_auc_score
from sklearn.preprocessing import MinMaxScaler
#import wfdb
import torchvision
import ast
import math
#import thop
import os.path
#import utils
import Plots
from torchvision import transforms
from PIL import Image
#np.set_printoptions(threshold=np.inf)
cwd = os.path.dirname(os.path.abspath(__file__))
mlb_path = os.path.join(cwd,  "PTB-XL", "ptb-xl", "output", "mlb.pkl")
scaler_path = os.path.join(cwd,  "PTB-XL", "ptb-xl", "output", "standard_scaler.pkl/")
ptb_path = os.path.join(cwd, "PTB-XL", "ptb-xl/")

import wandb

wandb.init(project="MNIST nonIID", entity="split-learning-medical")

f = open('parameter_client.json', )
data = json.load(f)

# set parameters fron json file
#epoch = data["training_epochs"]
lr = data["learningrate"]
batchsize = data["batchsize"]
batch_concat = data["batch_concat"]
host = data["host"]
port = data["port"]
max_recv = data["max_recv"]
autoencoder = data["autoencoder"]
detailed_output = data["detailed_output"]
count_flops = data["count_flops"]
plots = data["plots"]
autoencoder_train = data["autoencoder_train"]
deactivate_train_after_num_epochs = data["deactivate_train_after_num_epochs"]
grad_encode = data["grad_encode"]
train_gradAE_active = data["train_gradAE_active"]
deactivate_grad_train_after_num_epochs = data["deactivate_grad_train_after_num_epochs"]
pretrain_this_client = 0
pretrain_epochs = 30
class_count = 0
clients = 10
mix_data = 0
print("class_count:", class_count)
print("clients:", clients)
print("mix_data:", mix_data)

#wandb.init(config={
#  "learning_rate": lr,
#  #"epochs": epoch,
#  "batch_size": batchsize,
#    "autoencoder": autoencoder
#})

#wandb.config.update({"learning_rate": lr, "PC: ": 2})


def print_json():
    print("learningrate: ", lr)
    print("grad_encode: ", grad_encode)
    print("gradAE_train: ", train_gradAE_active)
    print("deactivate_grad_train_after_num_epochs: ", deactivate_grad_train_after_num_epochs)
    #print("Getting the metadata epoch: ", epoch)
    print("Getting the metadata host: ", host)
    print("Getting the metadata port: ", port)
    print("Getting the metadata batchsize: ", batchsize)
    print("Autoencoder: ", autoencoder)
    print("detailed_output: ", detailed_output)
    print("count_flops: ", count_flops)
    print("plots: ", plots)
    print("autoencoder_train: ", autoencoder_train)
    print("deactivate_train_after_num_epochs: ", deactivate_train_after_num_epochs)

# load data from json file
"""
class PTB_XL(Dataset):
    def __init__(self, stage=None):
        self.stage = stage
        if self.stage == 'train':
            global X_train
            global y_train
            self.y_train = y_train
            self.X_train = X_train
        if self.stage == 'val':
            global y_val
            global X_val
            self.y_val = y_val
            self.X_val = X_val
        if self.stage == 'test':
            global y_test
            global X_test
            self.y_test = y_test
            self.X_test = X_test

    def __len__(self):
        if self.stage == 'train':
            return len(self.y_train)
        if self.stage == 'val':
            return len(self.y_val)
        if self.stage == 'test':
            return len(self.y_test)

    def __getitem__(self, idx):
        if self.stage == 'train':
            sample = self.X_train[idx].transpose((1, 0)), self.y_train[idx]
        if self.stage == 'val':
            sample = self.X_val[idx].transpose((1, 0)), self.y_val[idx]
        if self.stage == 'test':
            sample = self.X_test[idx].transpose((1, 0)), self.y_test[idx]
        return sample
"""

transform = transforms.Compose([
    transforms.Resize((32, 32)),
    transforms.ToTensor(),
    transforms.Normalize((0.3403, 0.3121, 0.3214),
                         (0.2724, 0.2608, 0.2669))
])


# Create Datasets
class GTSRB(Dataset):
    base_folder = 'GTSRB'

    def __init__(self, root_dir, train=False, transform=None):
        self.root_dir = root_dir
        self.sub_directory = 'Final_Test/Images' if train else 'Final_Test/Images'
        self.csv_file_name = 'GT-final_test.test.csv' if train else 'GT-final_test.test.csv'

        csv_file_path = os.path.join(
            root_dir, self.base_folder, self.sub_directory, self.csv_file_name).replace("\\", "/")

        self.csv_data = pd.read_csv(csv_file_path, sep=';')
        self.transform = transform

    def __len__(self):
        return len(self.csv_data)

    def __getitem__(self, idx):
        img_path = os.path.join(self.root_dir, self.base_folder, self.sub_directory,
                                self.csv_data.iloc[idx, 0]).replace("\\", "/")

        img = Image.open(img_path)
        classId = self.csv_data.iloc[idx, -1]

        if self.transform is not None:
            img = self.transform(img)
        return img, classId


def init():
    """
    trainset = GTSRB(root_dir='.\GTSRB', train=True, transform=transform)
    testset = GTSRB(root_dir=".\GTSRB", train=False, transform=transform)

    global train_loader
    global val_loader
    global test_loader

    train_loader = torch.utils.data.DataLoader(trainset, batch_size=batchsize, shuffle=True, num_workers=2)
    test_loader = torch.utils.data.DataLoader(testset, batch_size=batchsize, shuffle=False, num_workers=2)
    val_loader = test_loader

    total_batch = len(train_loader)
    total_batch_train = len(train_loader)
    total_batch_test = len(test_loader)
    """
    #global train_loader
    global val_loader
    global test_loader
    global train_data
    global pretrain_loader
    global pretrain_dataset
    global nonIID_loader
    train_data = torchvision.datasets.MNIST('E:/MNIST',
                                            train=True, download=True,
                                            transform=torchvision.transforms.Compose([
                                                torchvision.transforms.ToTensor(),
                                                torchvision.transforms.Normalize(
                                                    (0.1307,), (0.3081,))
                                            ]))
    len_train_dataset = len(train_data)
    print(len_train_dataset)
    pretrain_dataset, nonIID_dataset = torch.utils.data.random_split(train_data,
                                                            [6000,
                                                             54000])#
    test1_dataset, test2_dataset = torch.utils.data.random_split(pretrain_dataset, [3000, 3000])  #

    pretrain_loader = torch.utils.data.DataLoader(pretrain_dataset, batch_size=batchsize, shuffle=True)
    nonIID_loader = torch.utils.data.DataLoader(nonIID_dataset, batch_size=batchsize, shuffle=True)


    #train_loader = torch.utils.data.DataLoader(train_data, batch_size=batchsize, shuffle=True)

    test_loader = torch.utils.data.DataLoader(
        torchvision.datasets.MNIST('E:/MNIST', train=False, download=True,
                                   transform=torchvision.transforms.Compose([
                                       torchvision.transforms.ToTensor(),
                                       torchvision.transforms.Normalize(
                                           (0.1307,), (0.3081,))
                                   ])),
        batch_size=batchsize, shuffle=True)
    val_loader = test_loader

    classes()
    """
    zero_loader = torch.utils.data.DataLoader(MNIST(clas="zero"), batch_size=batchsize, shuffle=True)
    one_loader = torch.utils.data.DataLoader(MNIST(clas="one"), batch_size=batchsize, shuffle=True)
    two_loader = torch.utils.data.DataLoader(MNIST(clas="two"), batch_size=batchsize, shuffle=True)
    three_loader = torch.utils.data.DataLoader(MNIST(clas="three"), batch_size=batchsize, shuffle=True)
    four_loader = torch.utils.data.DataLoader(MNIST(clas="four"), batch_size=batchsize, shuffle=True)
    five_loader = torch.utils.data.DataLoader(MNIST(clas="five"), batch_size=batchsize, shuffle=True)
    six_loader = torch.utils.data.DataLoader(MNIST(clas="six"), batch_size=batchsize, shuffle=True)
    seven_loader = torch.utils.data.DataLoader(MNIST(clas="seven"), batch_size=batchsize, shuffle=True)
    eight_loader = torch.utils.data.DataLoader(MNIST(clas="eight"), batch_size=batchsize, shuffle=True)
    nine_loader = torch.utils.data.DataLoader(MNIST(clas="nine"), batch_size=batchsize, shuffle=True)
    """
    global client1_loader, client2_loader, client3_loader, client4_loader, client5_loader
    global client6_loader, client7_loader, client8_loader, client9_loader, client10_loader
    if clients == 5:
        global one_loader
        one_data = MNIST(clas="zero")
        one_loader = torch.utils.data.DataLoader(one_data, batch_size=batchsize, shuffle=True)
        if class_count == 0:
            client1_dataset, client2_dataset = torch.utils.data.random_split(train_data, [12000, 48000])
            client2_dataset, client3_dataset = torch.utils.data.random_split(client2_dataset, [12000, 36000])
            client3_dataset, client4_dataset = torch.utils.data.random_split(client3_dataset, [12000, 24000])
            client4_dataset, client5_dataset = torch.utils.data.random_split(client4_dataset, [12000, 12000])
        if class_count == 1:
            client1_dataset = torch.utils.data.ConcatDataset((MNIST(clas="zero"), MNIST(clas="one")))
            print("client:", len(client1_dataset))
            # client1_loader = torch.utils.data.DataLoader(client1_dataset, batch_size=batchsize, shuffle=True)
            client2_dataset = torch.utils.data.ConcatDataset((MNIST(clas="two"), MNIST(clas="three")))
            print("client:", len(client2_dataset))
            # client2_loader = torch.utils.data.DataLoader(client2_dataset, batch_size=batchsize, shuffle=True)
            client3_dataset = torch.utils.data.ConcatDataset((MNIST(clas="four"), MNIST(clas="five")))
            print("client:", len(client3_dataset))
            # client3_loader = torch.utils.data.DataLoader(client3_dataset, batch_size=batchsize, shuffle=True)
            client4_dataset = torch.utils.data.ConcatDataset((MNIST(clas="six"), MNIST(clas="seven")))
            print("client:", len(client4_dataset))
            # client4_loader = torch.utils.data.DataLoader(client4_dataset, batch_size=batchsize, shuffle=True)
            client5_dataset = torch.utils.data.ConcatDataset((MNIST(clas="eight"), MNIST(clas="nine")))
            print("client:", len(client5_dataset))
            # client5_loader = torch.utils.data.DataLoader(client5_dataset, batch_size=batchsize, shuffle=True)

        if class_count == 2:
            zeroone = torch.utils.data.ConcatDataset((MNIST(clas="zero"), MNIST(clas="one")))
            twothree = torch.utils.data.ConcatDataset((MNIST(clas="two"), MNIST(clas="three")))
            fourfive = torch.utils.data.ConcatDataset((MNIST(clas="four"), MNIST(clas="five")))
            sixseven = torch.utils.data.ConcatDataset((MNIST(clas="six"), MNIST(clas="seven")))
            eightnine = torch.utils.data.ConcatDataset((MNIST(clas="eight"), MNIST(clas="nine")))

            client1_dataset = torch.utils.data.ConcatDataset((zeroone, twothree))
            client2_dataset = torch.utils.data.ConcatDataset((fourfive, sixseven))
            client3_dataset = torch.utils.data.ConcatDataset((eightnine, zeroone))
            client4_dataset = torch.utils.data.ConcatDataset((twothree, fourfive))
            client5_dataset = torch.utils.data.ConcatDataset((sixseven, eightnine))

        if class_count == 3:
            zeroone = torch.utils.data.ConcatDataset((MNIST(clas="zero"), MNIST(clas="one")))
            twothree = torch.utils.data.ConcatDataset((MNIST(clas="two"), MNIST(clas="three")))
            fourfive = torch.utils.data.ConcatDataset((MNIST(clas="four"), MNIST(clas="five")))
            sixseven = torch.utils.data.ConcatDataset((MNIST(clas="six"), MNIST(clas="seven")))
            eightnine = torch.utils.data.ConcatDataset((MNIST(clas="eight"), MNIST(clas="nine")))

            client1_dataset = torch.utils.data.ConcatDataset(
                (fourfive, torch.utils.data.ConcatDataset((zeroone, twothree))))
            client2_dataset = torch.utils.data.ConcatDataset(
                (eightnine, torch.utils.data.ConcatDataset((fourfive, sixseven))))
            client3_dataset = torch.utils.data.ConcatDataset(
                (twothree, torch.utils.data.ConcatDataset((eightnine, zeroone))))
            client4_dataset = torch.utils.data.ConcatDataset(
                (sixseven, torch.utils.data.ConcatDataset((twothree, fourfive))))
            client5_dataset = torch.utils.data.ConcatDataset(
                (zeroone, torch.utils.data.ConcatDataset((sixseven, eightnine))))

        client1_loader = torch.utils.data.DataLoader(client1_dataset, batch_size=batchsize, shuffle=True)
        client2_loader = torch.utils.data.DataLoader(client2_dataset, batch_size=batchsize, shuffle=True)
        client3_loader = torch.utils.data.DataLoader(client3_dataset, batch_size=batchsize, shuffle=True)
        client4_loader = torch.utils.data.DataLoader(client4_dataset, batch_size=batchsize, shuffle=True)
        client5_loader = torch.utils.data.DataLoader(client5_dataset, batch_size=batchsize, shuffle=True)
        print("client:", len(client1_dataset))
        print("client:", len(client2_dataset))
        print("client:", len(client3_dataset))
        print("client:", len(client4_dataset))
        print("client:", len(client5_dataset))



    if clients==10:
        if class_count == 0:
            client1_loader = torch.utils.data.DataLoader(randomdata(nonIID_dataset), batch_size=batchsize, shuffle=True)
            client2_loader = torch.utils.data.DataLoader(randomdata(nonIID_dataset), batch_size=batchsize, shuffle=True)
            client3_loader = torch.utils.data.DataLoader(randomdata(nonIID_dataset), batch_size=batchsize, shuffle=True)
            client4_loader = torch.utils.data.DataLoader(randomdata(nonIID_dataset), batch_size=batchsize, shuffle=True)
            client5_loader = torch.utils.data.DataLoader(randomdata(nonIID_dataset), batch_size=batchsize, shuffle=True)
            client6_loader = torch.utils.data.DataLoader(randomdata(nonIID_dataset), batch_size=batchsize, shuffle=True)
            client7_loader = torch.utils.data.DataLoader(randomdata(nonIID_dataset), batch_size=batchsize, shuffle=True)
            client8_loader = torch.utils.data.DataLoader(randomdata(nonIID_dataset), batch_size=batchsize, shuffle=True)
            client9_loader = torch.utils.data.DataLoader(randomdata(nonIID_dataset), batch_size=batchsize, shuffle=True)
            client10_loader = torch.utils.data.DataLoader(randomdata(nonIID_dataset), batch_size=batchsize, shuffle=True)
        if class_count == 1:
            if mix_data:
                client1_dataset = torch.utils.data.ConcatDataset((MNIST(clas="zero"), randompartofG()))
                client2_dataset = torch.utils.data.ConcatDataset((MNIST(clas="one"), randompartofG()))
                client3_dataset = torch.utils.data.ConcatDataset((MNIST(clas="two"), randompartofG()))
                client4_dataset = torch.utils.data.ConcatDataset((MNIST(clas="three"), randompartofG()))
                client5_dataset = torch.utils.data.ConcatDataset((MNIST(clas="four"), randompartofG()))
                client6_dataset = torch.utils.data.ConcatDataset((MNIST(clas="five"), randompartofG()))
                client7_dataset = torch.utils.data.ConcatDataset((MNIST(clas="six"), randompartofG()))
                client8_dataset = torch.utils.data.ConcatDataset((MNIST(clas="seven"), randompartofG()))
                client9_dataset = torch.utils.data.ConcatDataset((MNIST(clas="eight"), randompartofG()))
                client10_dataset = torch.utils.data.ConcatDataset((MNIST(clas="nine"), randompartofG()))
                client1_loader = torch.utils.data.DataLoader(client1_dataset, batch_size=batchsize, shuffle=True)
                client2_loader = torch.utils.data.DataLoader(client2_dataset, batch_size=batchsize, shuffle=True)
                client3_loader = torch.utils.data.DataLoader(client3_dataset, batch_size=batchsize, shuffle=True)
                client4_loader = torch.utils.data.DataLoader(client4_dataset, batch_size=batchsize, shuffle=True)
                client5_loader = torch.utils.data.DataLoader(client5_dataset, batch_size=batchsize, shuffle=True)
                client6_loader = torch.utils.data.DataLoader(client6_dataset, batch_size=batchsize, shuffle=True)
                client7_loader = torch.utils.data.DataLoader(client7_dataset, batch_size=batchsize, shuffle=True)
                client8_loader = torch.utils.data.DataLoader(client8_dataset, batch_size=batchsize, shuffle=True)
                client9_loader = torch.utils.data.DataLoader(client9_dataset, batch_size=batchsize, shuffle=True)
                client10_loader = torch.utils.data.DataLoader(client10_dataset, batch_size=batchsize, shuffle=True)
                print("client1:", len(client1_dataset))
            else:
                client1_loader = torch.utils.data.DataLoader(MNIST(clas="zero"), batch_size=batchsize, shuffle=True)
                client2_loader = torch.utils.data.DataLoader(MNIST(clas="one"), batch_size=batchsize, shuffle=True)
                client3_loader = torch.utils.data.DataLoader(MNIST(clas="two"), batch_size=batchsize, shuffle=True)
                client4_loader = torch.utils.data.DataLoader(MNIST(clas="three"), batch_size=batchsize, shuffle=True)
                client5_loader = torch.utils.data.DataLoader(MNIST(clas="four"), batch_size=batchsize, shuffle=True)
                client6_loader = torch.utils.data.DataLoader(MNIST(clas="five"), batch_size=batchsize, shuffle=True)
                client7_loader = torch.utils.data.DataLoader(MNIST(clas="six"), batch_size=batchsize, shuffle=True)
                client8_loader = torch.utils.data.DataLoader(MNIST(clas="seven"), batch_size=batchsize, shuffle=True)
                client9_loader = torch.utils.data.DataLoader(MNIST(clas="eight"), batch_size=batchsize, shuffle=True)
                client10_loader = torch.utils.data.DataLoader(MNIST(clas="nine"), batch_size=batchsize, shuffle=True)
                print("client1:", len(MNIST(clas="zero")))


def randomdata(nonIID_dataset):
    randomdata, nodata = torch.utils.data.random_split(nonIID_dataset, [6000, 48000])
    return randomdata

class randompartofG(Dataset):
    def __init__(self):
        randompartofG, rest = torch.utils.data.random_split(pretrain_dataset, [3000, 3000])
        randompartofG_loader = torch.utils.data.DataLoader(randompartofG, batch_size=batchsize, shuffle=True)
        self.x, self.y = [],[]
        for b, batch in enumerate(randompartofG_loader):
            x_temp, y_temp = batch
            for i in range(len(batch)):
                self.x.append(x_temp[i])
                self.y.append(y_temp[i])

    def __len__(self):
        return len(self.x)
    def __getitem__(self, idx):
        return self.x[idx], self.y[idx]

class MNIST(Dataset):
    def __init__(self, clas):
        if clas == "zero":
            self.x_train = zero_x
            self.y_train = zero_y
        if clas == "one":
            self.x_train = one_x
            self.y_train = one_y
        if clas == "two":
            self.x_train = two_x
            self.y_train = two_y
        if clas == "three":
            self.x_train = three_x
            self.y_train = three_y
        if clas == "four":
            self.x_train = four_x
            self.y_train = four_y
        if clas == "five":
            self.x_train = five_x
            self.y_train = five_y
        if clas == "six":
            self.x_train = six_x
            self.y_train = six_y
        if clas == "seven":
            self.x_train = seven_x
            self.y_train = seven_y
        if clas == "eight":
            self.x_train = eight_x
            self.y_train = eight_y
        if clas == "nine":
            self.x_train = nine_x
            self.y_train = nine_y


    def __len__(self):
        return len(self.x_train)

    def __getitem__(self, idx):
        return self.x_train[idx], self.y_train[idx]

"""
def init():
    train_dataset = PTB_XL('train')
    val_dataset = PTB_XL('val')
    global train_loader
    global val_loader
    train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=batchsize, shuffle=True)
    val_loader = torch.utils.data.DataLoader(val_dataset, batch_size=batchsize, shuffle=True)
"""
"""
def new_split():
    global train_loader
    global val_loader
    train_dataset, val_dataset = torch.utils.data.random_split(training_dataset,
                                                               [size_train_dataset,
                                                                len(training_dataset) - size_train_dataset])
    print("train_dataset size: ", size_train_dataset)
    print("val_dataset size: ", len(training_dataset) - size_train_dataset)
    train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=batchsize, shuffle=True)
    val_loader = torch.utils.data.DataLoader(val_dataset, batch_size=batchsize, shuffle=True)
"""

if count_flops: #Does not work on the Jetson Nano yet. The amount of FLOPs doesn't depend on the architecture. Measuring FLOPs on the PC and JetsonNano would result in the same outcome.
    # The paranoid switch prevents the FLOPs count
    # Solution: sudo sh -c 'echo 1 >/proc/sys/kernel/perf_event_paranoid'
    # Needs to be done after every restart of the PC
    from ptflops import get_model_complexity_info
    from pypapi import events, papi_high as high


def classes():
    for b, batch in enumerate(nonIID_loader):
        x, y = batch
        for i in range(len(x)):
            if y[i] == 0:
                zero_x.append(x[i])
                zero_y.append(y[i])
            if y[i] == 1:
                one_x.append(x[i])
                one_y.append(y[i])
            if y[i] == 2:
                two_x.append(x[i])
                two_y.append(y[i])
            if y[i] == 3:
                three_x.append(x[i])
                three_y.append(y[i])
            if y[i] == 4:
                four_x.append(x[i])
                four_y.append(y[i])
            if y[i] == 5:
                five_x.append(x[i])
                five_y.append(y[i])
            if y[i] == 6:
                six_x.append(x[i])
                six_y.append(y[i])
            if y[i] == 7:
                seven_x.append(x[i])
                seven_y.append(y[i])
            if y[i] == 8:
                eight_x.append(x[i])
                eight_y.append(y[i])
            if y[i] == 9:
                nine_x.append(x[i])
                nine_y.append(y[i])

    print("ones:", len(one_x))
    print("twos:", len(two_x))
    print("threes:", len(three_x))
    print("fours:", len(four_x))
    print("fives:", len(five_x))
    print("sixes:", len(six_x))
    print("sevens:", len(seven_x))
    print("eights:", len(eight_x))
    print("nines:", len(nine_x))

def str_to_number(label):
    a = np.zeros(5)
    if not label:
        return a
    for i in label:
        if i == 'NORM':
            a[0] = 1
        if i == 'MI':
            a[1] = 1
        if i == 'STTC':
            a[2] = 1
        if i == 'HYP':
            a[3] = 1
        if i == 'CD':
            a[4] = 1
    return a


class ClientMNIST(nn.Module):
    """
    client model
    """
    def __init__(self):
        super(ClientMNIST, self).__init__()

        self.cnn1 = nn.Conv2d(in_channels=1, out_channels=4, kernel_size=5, stride=1, padding=0)
        self.relu1 = nn.ReLU()
        self.maxpool1 = nn.MaxPool2d(kernel_size=2)

    def forward(self, x):
        x = self.cnn1(x)
        x = self.relu1(x)
        x = self.maxpool1(x)
        return x



class Client(nn.Module):
    """
    Client-Model:
    """
    def __init__(self, training=True):
        super(Client, self).__init__()
        self.conv1 = nn.Conv1d(12, 192, kernel_size=3, stride=2, dilation=1, padding=1)
        nn.init.kaiming_normal_(self.conv1.weight, mode='fan_out', nonlinearity='relu')
        self.relu1 = nn.ReLU()
        self.pool1 = nn.MaxPool1d(kernel_size=3, stride=2)
        self.drop1 = nn.Dropout(0.4, training)
        self.conv2 = nn.Conv1d(192, 192, kernel_size=3, stride=2, dilation=1, padding=1)
        nn.init.kaiming_normal_(self.conv2.weight, mode='fan_out', nonlinearity='relu')
        self.relu2 = nn.ReLU()
        self.pool2 = nn.MaxPool1d(kernel_size=3, stride=2)

    def forward(self, x, drop=True):
        x = self.conv1(x)
        x = self.relu1(x)
        x = self.pool1(x)
        if drop == True: x = self.drop1(x)
        x = self.conv2(x)
        x = self.relu2(x)
        x = self.pool2(x)
        return x


class Encode(nn.Module):
    """
    encoder model
    """
    def __init__(self):
        super(Encode, self).__init__()
        self.conva = nn.Conv1d(192, 144, 2, stride=2,  padding=1)
        self.convb = nn.Conv1d(144, 96, 2, stride=2, padding=0)
        self.convc = nn.Conv1d(96, 48, 2, stride=2,  padding=0)
        self.convd = nn.Conv1d(48, 24, 2, stride=2, padding=0)##

    def forward(self, x):
        x = self.conva(x)
        #print("encode 1 Layer: ", x.size())
        x = self.convb(x)
        #print("encode 2 Layer: ", x.size())
        x = self.convc(x)
        #print("encode 3 Layer: ", x.size())
        x = self.convd(x)
        #print("encode 4 Layer: ", x.size())
        #print("encode 5 Layer: ", x.size())
        return x


class EncodeMNIST(nn.Module):
    """
    encoder model
    """
    def __init__(self):
        super(EncodeMNIST, self).__init__()
        self.conva = nn.Conv2d(4, 4, 3)
        self.convb = nn.Conv2d(4, 2, 3)

    def forward(self, x):
        x = self.conva(x)
        #print("encode 1 Layer: ", x.size())
        x = self.convb(x)
        #print("encode 2 Layer: ", x.size())
        return x

class EncodeMNIST2(nn.Module):
    """
    encoder model
    """
    def __init__(self):
        super(EncodeMNIST2, self).__init__()
        self.conva = nn.Conv2d(4, 16, 3)
        self.convb = nn.Conv2d(16, 8, 3)
        self.convc = nn.Conv2d(8, 2, 3)

    def forward(self, x):
        x = self.conva(x)
        #print("encode 1 Layer: ", x.size())
        x = self.convb(x)
        x = self.convc(x)
        #print("encode 2 Layer: ", x.size())
        return x


class Grad_Decoder(nn.Module):
    """
    decoder model
    """
    def __init__(self):
        super(Grad_Decoder, self).__init__()
        self.t_convb = nn.ConvTranspose1d(24, 48, 2, stride=2, padding=0)
        self.t_convc = nn.ConvTranspose1d(48, 96, 2, stride=2, padding=0)
        self.t_convd = nn.ConvTranspose1d(96, 144, 2, stride=2, padding=0)
        self.t_conve = nn.ConvTranspose1d(144, 192, 2, stride=2, padding=1)

    def forward(self, x):
        #print("decode 1 Layer: ", x.size())
        x = self.t_convb(x)
        #print("decode 2 Layer: ", x.size())
        x = self.t_convc(x)
        #print("decode 3 Layer: ", x.size())
        x = self.t_convd(x)
        #print("decode 4 Layer: ", x.size())
        x = self.t_conve(x)
        #print("decode 4 Layer: ", x.size())
        return x


class DecodeGradMNIST(nn.Module):
    """
    decoder model
    """
    def __init__(self):
        super(DecodeGradMNIST, self).__init__()
        self.t_convb = nn.ConvTranspose2d(16, 128, 3)
        self.t_convc = nn.ConvTranspose2d(128, 16, 3)


    def forward(self, x):
        x = self.t_convb(x)
        #print("decode 2 Layer: ", x.size())
        x = self.t_convc(x)
        #print("decode 3 Layer: ", x.size())
        return x

class DecodeGradMNIST2(nn.Module):
    """
    decoder model
    """
    def __init__(self):
        super(DecodeGradMNIST2, self).__init__()
        self.t_convb = nn.ConvTranspose2d(4, 64, 3)
        self.t_convc = nn.ConvTranspose2d(64, 128, 3)
        self.t_convd = nn.ConvTranspose2d(128, 4, 3)


    def forward(self, x):
        x = self.t_convb(x)
        #print("decode 2 Layer: ", x.size())
        x = self.t_convc(x)
        x = self.t_convd(x)
        #print("decode 3 Layer: ", x.size())
        return x



#send/recieve system:
def send_msg(sock, getid, content):
    """
    pickles the content (creates bitstream), adds header and send message via tcp port

    :param sock: socket
    :param content: content to send via tcp port
    """
    msg = [getid, content]  # add getid
    msg = pickle.dumps(msg)
    msg = struct.pack('>I', len(msg)) + msg  # add 4-byte length in network byte order
    #print("communication overhead send: ", sys.getsizeof(msg), " bytes")
    global data_send_per_epoch
    data_send_per_epoch += sys.getsizeof(msg)
    sock.sendall(msg)


def recieve_msg(sock):
    """
    recieves the meassage with helper function, umpickles the message and separates the getid from the actual massage content
    :param sock: socket
    """

    msg = recv_msg(sock)  # receive client message from socket
    msg = pickle.loads(msg)
    return msg


def recieve_request(sock):
    """
    recieves the meassage with helper function, umpickles the message and separates the getid from the actual massage content
    :param sock: socket
    """

    msg = recv_msg(sock)  # receive client message from socket
    msg = pickle.loads(msg)
    getid = msg[0]
    content = msg[1]
    handle_request(sock, getid, content)


def recv_msg(sock):
    """
    gets the message length (which corresponds to the first
    4 bytes of the recieved bytestream) with the recvall function

    :param sock: socket
    :return: returns the data retrieved from the recvall function
    """
    # read message length and unpack it into an integer
    raw_msglen = recvall(sock, 4)
    if not raw_msglen:
        return None
    msglen = struct.unpack('>I', raw_msglen)[0]

    #print("Message length:", msglen)
    global data_recieved_per_epoch
    data_recieved_per_epoch += msglen
    # read the message data
    return recvall(sock, msglen)


def recvall(sock, n):
    """
    returns the data from a recieved bytestream, helper function
    to receive n bytes or return None if EOF is hit
    :param sock: socket
    :param n: length in bytes (number of bytes)
    :return: message
    """
    #
    data = b''
    while len(data) < n:
        if detailed_output:
            print("Start function sock.recv")
        packet = sock.recv(n - len(data))
        if not packet:
            return None
        data += packet
    # print("Daten: ", data)
    return data


def handle_request(sock, getid, content):
    """
    executes the requested function, depending on the get id, and passes the recieved message

    :param sock: socket
    :param getid: id of the function, that should be executed if the message is recieved
    :param content: message content
    """
    #print("request mit id:", getid)
    switcher = {
        0: initialize_model,
        1: train_epoch,
        2: val_stage,
        3: test_stage,
    }
    switcher.get(getid, "invalid request recieved")(sock, content)


def serverHandler(conn):
    while True:
        recieve_request(conn)


def start_training(s, content):
    """
    actuall function, which does the training from the train loader and
    testing from the testloader epoch/batch wise
    :param s: socket
    """
    train_losses = []
    train_accs = []
    total_f1 = []
    val_losses = []
    val_accs = []
    data_send_per_epoch_total = []
    data_recieved_per_epoch_total = []
    batches_abort_rate_total = []
    time_train_val = 0
    train_losses.append(0)
    train_accs.append(0)
    val_losses.append(0)
    val_accs.append(0)
    total_f1.append(0)



    start_time_training = time.time()
    for e in range(epoch):
        #print(f"Starting epoch: {e}/{epoch}")
        if e >= deactivate_train_after_num_epochs: #condition to stop AE training
            train_active = 0 #AE training off
        if e >= deactivate_grad_train_after_num_epochs:
            train_grad_active = 0
        if e == 1:
            print("estimated_time_total: ", time_train_val*epoch/60, " min")
        if count_flops:
            x = high.read_counters()# reset Flop counter

        train_epoch(s)

        initial_weights = client.state_dict()
        send_msg(s, 2, initial_weights)

        msg = 0

        send_msg(s, 3, msg)

        if autoencoder:
            if autoencoder_train:
                print("Autoencoder_train status: ", train_active)
            if grad_encode:
                print("Grad AE train status: ", train_grad_active)

        #batches_abort_rate_total.append(batches_aborted / total_train_nr)


    total_training_time = time.time() - start_time_training
    time_info = "trainingtime for {} epochs: {:.2f}min".format(epoch, total_training_time / 60)
    print("\n", time_info)
    print("Start testing:")

    test_stage(s)


    data_transfer_per_epoch = 0
    average_dismissal_rate = 0
    for data in data_send_per_epoch_total:
        data_transfer_per_epoch += data
    for data in data_recieved_per_epoch_total:
        data_transfer_per_epoch += data
    for data in batches_abort_rate_total:
        average_dismissal_rate += data
    print("Average data transfer/epoch: ", data_transfer_per_epoch/epoch/1000000, " MB")
    print("Average dismissal rate: ", average_dismissal_rate/epoch)
    total_flops_forward = 0
    total_flops_encoder = 0
    total_flops_backprob = 0
    #for flop in flops_client_forward_total:
    #    total_flops_forward += flop[0]
    #for flop in flops_client_encoder_total:
    #    total_flops_encoder += flop[0]
    #for flop in flops_client_backprob_total:
    #    total_flops_backprob += flop[0]
    print("total FLOPs forward: ", total_flops_forward)
    print("total FLOPs encoder: ", total_flops_encoder)
    print("total FLOPs backprob: ", total_flops_backprob)
    print("total FLOPs client: ", total_flops_backprob+total_flops_encoder+total_flops_forward)
    #plot(val_accs, train_accs, train_losses, val_losses, total_f1)
    #plt.show()


def grad_postprocessing(grad):
    grad_new = grad.numpy()
    for a in range(64):
        for b in range(4):
            #scaler.fit(grad[a])
            grad_new[a][b] = scaler.inverse_transform(grad[a][b])
    grad_new = torch.FloatTensor(grad_new)
    return grad_new


def train_epoch(s, pretraining):
    #new_split() #new random dist between train and val
    loss_grad_total = 0
    global epoch
    epoch += 1
    flops_forward_epoch, flops_encoder_epoch, flops_backprop_epoch, flops_rest, flops_send, flops_recieve = 0,0,0,0,0,0
    #Specify AE configuration
    train_active = 0 #default: AE is pretrained
    train_grad_active = 0
    if epoch < deactivate_train_after_num_epochs:
        if autoencoder_train:
            train_active = 1
    if epoch < deactivate_grad_train_after_num_epochs:
        if train_gradAE_active:
            train_grad_active = 1
    #if pretraining:
    #    train_active = 1
    if train_active == 1:
        print("AE train active")

    global data_send_per_epoch, data_recieved_per_epoch, data_send_per_epoch_total, data_recieved_per_epoch_total
    data_send_per_epoch, data_recieved_per_epoch = 0, 0
    correct_train, total_train, train_loss = 0, 0, 0
    batches_aborted, total_train_nr, total_val_nr, total_test_nr = 0, 0, 0, 0
    hamming_epoch, precision_epoch, recall_epoch, f1_epoch, auc_train = 0, 0, 0, 0, 0
    #encoder_grad_server = 0

    epoch_start_time = time.time()
    """
    train_dataset, val_data = torch.utils.data.random_split(train_data,
                                                            [5000,
                                                             55000])
    print(len(train_dataset))
    train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=batchsize, shuffle=True)
    """
    a=10 if clients==10 else 5
    if pretraining:
        a = 1

    #a = 1
    ##client1_loader = torch.utils.data.DataLoader(train_data, batch_size=batchsize, shuffle=True)

    for i in range(a):
        if i == 0:
            train_loader = client1_loader
        if i == 1:
            train_loader = client2_loader
        if i == 2:
            train_loader = client3_loader
        if i == 3:
            train_loader = client4_loader
        if i == 4:
            train_loader = client5_loader
        if i == 5:
            train_loader = client6_loader
        if i == 6:
            train_loader = client7_loader
        if i == 7:
            train_loader = client8_loader
        if i == 8:
            train_loader = client9_loader
        if i == 9:
            train_loader = client10_loader
        #print("i: ", i)

        if pretraining:
            train_loader = pretrain_loader
        #if class_count == 0:
        #    train_loader = pretrain_loader

        for b, batch in enumerate(train_loader):
            if count_flops:
                x = high.read_counters()
            # print("batch: ", b)
            # print("FLOPs dataloader: ", x)
            # if b % 100 == 0:
            # print("batch ", b, " / ", total_batch)

            forward_time = time.time()
            active_training_time_batch_client = 0
            start_time_batch_forward = time.time()

            # define labels and data per batch
            x_train, label_train = batch
            x_train = x_train.to(device)

            #if b == 1:
            #    print("client_nr:", i+1)
            #    print("label: ", label_train[0])
            # x_train = x_train.to(device)
            # label_train = label_train.double().to(device)
            label_train = label_train.to(device)

            if len(x_train) != 64:
                break

            if count_flops:
                x = high.read_counters()
                flops_rest += x[0]  # reset Flop Counter
            optimizer.zero_grad()  # sets gradients to 0 - start for backprop later

            client_output_backprop = client(x_train)
            client_output_train = client_output_backprop.detach().clone()

            if count_flops:
                x = high.read_counters()
                # print("FLOPs forward: ", x)
                flops_forward_epoch += x[0]

            client_output_train_without_ae_send = 0
            if autoencoder:
                if train_active:
                    optimizerencode.zero_grad()
                # client_output_train_without_ae = client_output_train.clone().detach().requires_grad_(False)
                client_encoded = encode(client_output_train)
                client_output_send = client_encoded.detach().clone()
                if train_active:
                    client_output_train_without_ae_send = client_output_train.detach().clone()
            else:
                client_output_send = client_output_train.detach().clone()
            # client_output_send = encode(client_output_train)

            if count_flops:
                x = high.read_counters()
                flops_encoder_epoch += x[0]

            global encoder_grad_server
            msg = {
                'client_output_train': client_output_send,
                'client_output_train_without_ae': client_output_train_without_ae_send,
                'label_train': label_train,  # concat_labels,
                'batch_concat': batch_concat,
                'batchsize': batchsize,
                'train_active': train_active,
                'encoder_grad_server': encoder_grad_server,
                'train_grad_active': train_grad_active,
                'grad_encode': grad_encode
            }
            active_training_time_batch_client += time.time() - start_time_batch_forward
            if detailed_output:
                print("Send the message to server")
            send_msg(s, 0, msg)

            if count_flops:
                x = high.read_counters()  # reset counter
                flops_send += x[0]

            # while concat_counter_recv < concat_counter_send:
            msg = recieve_msg(s)
            client_grad_without_encode = msg["client_grad_without_encode"]
            client_grad = msg["grad_client"]
            # print("msg: ", msg)
            wandb.log({"dropout_threshold": msg["dropout_threshold"]},
                      commit=False)

            if count_flops:
                x = high.read_counters()  # reset counter
                flops_recieve += x[0]

            # decode grad:
            global scaler
            scaler = msg["scaler"]
            if msg["grad_encode"]:
                if msg["client_grad_abort"] == 1:
                    client_grad_decode = client_grad  # .detach().clone()
                else:
                    if train_grad_active:
                        # print("train_active")
                        optimizer_grad_decoder.zero_grad()
                    client_grad = Variable(client_grad, requires_grad=True)
                    client_grad_decode = grad_decoder(client_grad)
                    if train_grad_active:
                        loss_grad_autoencoder = error_grad_autoencoder(client_grad_without_encode, client_grad_decode)
                        loss_grad_total += loss_grad_autoencoder.item()
                        loss_grad_autoencoder.backward()
                        encoder_grad_server = client_grad.grad.detach().clone()  #
                        optimizer_grad_decoder.step()
                        # print("loss_grad_autoencoder: ", loss_grad_autoencoder)
                    else:
                        encoder_grad_server = 0
                    client_grad_decode = grad_postprocessing(client_grad_decode.detach().clone().cpu())
            else:
                if msg["client_grad_abort"] == 0:
                    client_grad_decode = client_grad.detach().clone()
                # else:
                #    client_grad = "abort"
                encoder_grad_server = 0

            start_time_batch_backward = time.time()

            encoder_grad = msg["encoder_grad"]
            if client_grad == "abort":
                # print("client_grad: ", client_grad)
                train_loss_add, add_correct_train, add_total_train = msg["train_loss"], msg["add_correct_train"], \
                                                                     msg["add_total_train"]
                correct_train += add_correct_train
                total_train_nr += 1
                total_train += add_total_train
                train_loss += train_loss_add
                batches_aborted += 1

                output_train = msg["output_train"]
                # print("train_loss: ", train_loss/total_train_nr)
                # meter.update(output_train, label_train, train_loss/total_train_nr)
                pass
            else:
                if train_active:
                    client_encoded.backward(encoder_grad)
                    optimizerencode.step()

                # concat_tensors[concat_counter_recv].to(device)
                # concat_tensors[concat_counter_recv].backward(client_grad)
                # client_output_backprob.to(device)
                # if b % 1000 == 999:
                #    print("Backprop with: ", client_grad)
                if count_flops:
                    x = high.read_counters()  # reset counter
                    flops_rest += x[0]

                client_output_backprop.backward(client_grad_decode)
                optimizer.step()

                if count_flops:
                    x = high.read_counters()
                    # print("FLOPs backprob: ", x)
                    flops_backprop_epoch += x[0]

                train_loss_add, add_correct_train, add_total_train = msg["train_loss"], msg["add_correct_train"], \
                                                                     msg["add_total_train"]

                correct_train += add_correct_train
                total_train_nr += 1
                total_train += add_total_train
                train_loss += train_loss_add

                output_train = msg["output_train"]
                # print("train_loss: ", train_loss/total_train_nr)
                # meter.update(output_train, label_train, train_loss/total_train_nr)

            # wandb.watch(client, log_freq=100)
            # output = torch.round(output_train)
            # if np.sum(label.cpu().detach().numpy()[0]) > 1:
            #    if np.sum(output.cpu().detach().numpy()[0] > 1):
            #        print("output[0]: ", output.cpu().detach().numpy()[0])
            #        print("label [0]: ", label.cpu().detach().numpy()[0])
            # if (total_train_nr % 50 == 0):
            #    print("output[0]: ", output_train.cpu().detach().numpy()[0])
            #    print("label [0]: ", label_train.cpu().detach().numpy()[0])

            # global batches_abort_rate_total
            # batches_abort_rate_total.append(batches_aborted / total_train_nr)

            active_training_time_batch_client += time.time() - start_time_batch_backward
            # active_training_time_batch_server = msg["active_trtime_batch_server"]
            # active_training_time_epoch_client += active_training_time_batch_client
            # active_training_time_epoch_server += active_training_time_batch_server
            #
            # try:
            #    roc_auc = roc_auc_score(label_train.detach().clone().cpu(), torch.round(output).detach().clone().cpu(), average='micro')
            #    auc_train += roc_auc
            # except:
            #    # print("auc_train_exception: ")
            # print("label: ", label)
            # print("output: ", output)
            #    pass
            # print(label_train)
            # print(output_train)
            pred = output_train.data.max(1, keepdim=True)[1]
            hamming_epoch += pred.eq(label_train.data.view_as(pred)).sum()

            hamming_epoch += 0  # accuracy_score(label_train.detach().clone().cpu(), output_train.detach().clone().cpu())# Plots.Accuracy(label_train.detach().clone().cpu(), output_train.detach().clone().cpu())#
            precision_epoch += 0  # precision_score(label_train.detach().clone().cpu(),
            #                                   output.detach().clone().cpu(), average='micro')
            recall_epoch += 0  # recall_score(label_train.detach().clone().cpu(), output.detach().clone().cpu(), average='micro')
            f1_epoch += 0  # f1_score(label_train.detach().clone().cpu(), output.detach().clone().cpu(), average='micro')

    flops_client_forward_total.append(flops_forward_epoch)
    flops_client_encoder_total.append(flops_encoder_epoch)
    flops_client_backprop_total.append(flops_backprop_epoch)
    flops_client_send_total.append(flops_send)
    flops_client_recieve_total.append(flops_recieve)
    flops_client_rest_total.append(flops_rest)


    print("data_send_per_epoch: ", data_send_per_epoch / 1000000, " MegaBytes")
    print("data_recieved_per_epoch: ", data_recieved_per_epoch / 1000000, "MegaBytes")
    data_send_per_epoch_total.append(data_send_per_epoch)
    data_recieved_per_epoch_total.append(data_recieved_per_epoch)

    epoch_endtime = time.time() - epoch_start_time
    #status_epoch_train = "epoch: {}, AUC_train: {:.4f}, Accuracy_micro: {:.4f}, Precision: {:.4f}, Recall: {:.4f}, F1: {:.4f}, trainingtime for epoch: {:.6f}s, batches abortrate:{:.2f}, train_loss: {:.4f}  ".format(
    #    epoch, auc_train / total_train_nr, hamming_epoch / (total_train_nr*64), precision_epoch / total_train_nr,
    #   recall_epoch / total_train_nr,
    #    f1_epoch / total_train_nr, epoch_endtime, batches_aborted / total_train_nr, train_loss / total_train_nr)
    if pretraining:
        print("pretrain ative")
    status_epoch_train = "epoch: {}, Accuracy: {:.4f}, trainingtime for epoch: {:.6f}s, batches abortrate: {:.4f}".format(epoch, hamming_epoch / (total_train_nr*64), epoch_endtime, batches_aborted / total_train_nr)
    print("status_epoch_train: ", status_epoch_train)
    #print("MegaFLOPS_forward_epoch", flops_forward_epoch/1000000)
    #print("MegaFLOPS_encoder_epoch", flops_encoder_epoch/1000000)
    #print("MegaFLOPS_backprop_epoch", flops_backprop_epoch/1000000)
    #print("MegaFLOPS_rest", flops_rest/1000000)
    #print("MegaFLOPS_send", flops_send/1000000)
    #print("MegaFLOPS_recieve", flops_recieve/1000000)
    wandb.log({"Batches Abortrate": batches_aborted / total_train_nr}, commit=False)
    #wandb.log({"Batches Abortrate": batches_aborted / total_train_nr, "MegaFLOPS Client Encoder": flops_encoder_epoch/1000000,
    #           "MegaFLOPS Client Forward": flops_forward_epoch / 1000000,
    #           "MegaFLOPS Client Backprop": flops_backprop_epoch / 1000000, "MegaFLOPS Send": flops_send / 1000000,
    #           "MegaFLOPS Recieve": flops_recieve / 1000000},
    #          commit=False)

    global auc_train_log
    auc_train_log = auc_train / total_train_nr
    global accuracy_train_log
    accuracy_train_log = hamming_epoch / total_train_nr
    global batches_abort_rate_total
    batches_abort_rate_total.append(batches_aborted / total_train_nr)

    initial_weights = client.state_dict()
    send_msg(s, 2, initial_weights)

    msg = 0

    send_msg(s, 3, msg)


def val_stage(s, pretraining=0):
    total_val_nr, val_loss_total, correct_val, total_val = 0, 0, 0, 0
    val_losses, val_accs  = [], []
    hamming_epoch, precision_epoch, recall_epoch, f1_epoch, accuracy, auc_val, accuracy_sklearn,  accuracy_custom = 0, 0, 0, 0, 0, 0, 0, 0
    val_time = time.time()
    with torch.no_grad():
        for b_t, batch_t in enumerate(val_loader):

            x_val, label_val = batch_t
            x_val, label_val = x_val.to(device), label_val.to(device)#label_val.double().to(device)
            optimizer.zero_grad()
            output_val = client(x_val)
            client_output_val = output_val.clone().detach().requires_grad_(True)
            if autoencoder:
                client_output_val = encode(client_output_val)

            msg = {'client_output_val/test': client_output_val,
                   'label_val/test': label_val,
                   }
            if detailed_output:
                print("The msg is:", msg)
            send_msg(s, 1, msg)
            if detailed_output:
                print("294: send_msg success!")
            msg = recieve_msg(s)
            if detailed_output:
                print("296: recieve_msg success!")
            correct_val_add = msg["correct_val/test"]
            val_loss = msg["val/test_loss"]
            output_val_server = msg["output_val/test_server"]
            val_loss_total += val_loss
            correct_val += correct_val_add
            total_val_add = len(label_val)
            total_val += total_val_add
            total_val_nr += 1

            #try:
            #    roc_auc = roc_auc_score(label_val.detach().clone().cpu(), torch.round(output_val_server).detach().clone().cpu(), average='micro')
            #    auc_val += roc_auc
            #except:
            #    # print("auc_train_exception: ")
            #    # print("label: ", label)
            #    # print("output: ", output)
            #    pass
            pred = output_val_server.data.max(1, keepdim=True)[1]
            accuracy_custom += pred.eq(label_val.data.view_as(pred)).sum()

            #output_val_server = torch.round(output_val_server)
            accuracy_sklearn += 0#Plots.Accuracy(label_val.detach().clone().cpu(),
                                #            output_val_server.detach().clone().cpu())
            accuracy_custom += 0#Plots.Accuracy(label_val.detach().clone().cpu(),
                                #            output_val_server.detach().clone().cpu())
            precision_epoch += 0#precision_score(label_val.detach().clone().cpu(),
                                #               output_val_server.detach().clone().cpu(), average='micro',
                                 #              zero_division=0)
            recall_epoch += 0#recall_score(label_val.detach().clone().cpu(), output_val_server.detach().clone().cpu(),
                             #            average='micro', zero_division=0)
            f1_epoch += 0#f1_score(label_val.detach().clone().cpu(), output_val_server.detach().clone().cpu(),
                         #        average='micro', zero_division=0)

    if pretraining == 0:
        wandb.log({"Loss_val": val_loss_total / total_val_nr,
                   "Accuracy_val_micro": accuracy_custom / (total_val_nr * 64)})  # ,
        # "F1_val": f1_epoch / total_val_nr,
        # "AUC_val": auc_val / total_val_nr,
        # "AUC_train": auc_train_log,
        # "Accuracy_train": accuracy_train_log})

    status_epoch_val = "epoch: {},AUC_val: {:.4f} ,Accuracy: {:.4f}, Precision: {:.4f}, Recall: {:.4f}, F1: {:.4f}, val_loss: {:.4f}".format(
        epoch, auc_val / total_val_nr, accuracy_custom / (total_val_nr*64), precision_epoch / total_val_nr,
        recall_epoch / total_val_nr,
        f1_epoch / total_val_nr, val_loss_total / total_val_nr)
    status_epoch_val = "epoch: {}, Accuracy: {:.4f}".format(epoch, accuracy_custom / (total_val_nr*64))
    print("status_epoch_val: ", status_epoch_val)

    msg = 0
    send_msg(s, 3, msg)


def test_stage(s, epoch):
    loss_test = 0.0
    correct_test, total_test = 0, 0
    hamming_epoch = 0
    precision_epoch = 0
    recall_epoch = 0
    f1_epoch = 0
    total_test_nr = 0
    with torch.no_grad():
        for b_t, batch_t in enumerate(val_loader):
            x_test, label_test = batch_t
            x_test, label_test = x_test.to(device), label_test.to(device)#label_test.double().to(device)
            optimizer.zero_grad()
            output_test = client(x_test)
            client_output_test = output_test.clone().detach().requires_grad_(True)
            if autoencoder:
                client_output_test = encode(client_output_test)

            msg = {'client_output_val/test': client_output_test,
                   'label_val/test': label_test,
                   }
            if detailed_output:
                print("The msg is:", msg)
            send_msg(s, 1, msg)
            if detailed_output:
                print("294: send_msg success!")
            msg = recieve_msg(s)
            if detailed_output:
                print("296: recieve_msg success!")
            correct_test_add = msg["correct_val/test"]
            test_loss = msg["val/test_loss"]
            output_test_server = msg["output_val/test_server"]
            loss_test += test_loss
            correct_test += correct_test_add
            total_test_add = len(label_test)
            total_test += total_test_add
            total_test_nr += 1

            #output_test_server = torch.round(output_test_server)
            hamming_epoch += Plots.Accuracy(label_test.detach().clone().cpu(),
                                            output_test_server.detach().clone().cpu())
            precision_epoch += 0#precision_score(label_test.detach().clone().cpu(),
                             #                  output_test_server.detach().clone().cpu(), average='micro')
            recall_epoch += 0#recall_score(label_test.detach().clone().cpu(), output_test_server.detach().clone().cpu(),
            #                             average='micro')
            f1_epoch += 0#f1_score(label_test.detach().clone().cpu(), output_test_server.detach().clone().cpu(), average='micro')

    status_test = "test: hamming_epoch: {:.4f}, precision_epoch: {:.4f}, recall_epoch: {:.4f}, f1_epoch: {:.4f}".format(
        hamming_epoch / total_test_nr, precision_epoch / total_test_nr, recall_epoch / total_test_nr,
        f1_epoch / total_test_nr)
    print("status_test: ", status_test)


    global data_send_per_epoch_total
    global data_recieved_per_epoch_total
    global batches_abort_rate_total


    data_transfer_per_epoch, average_dismissal_rate, total_flops_forward, total_flops_encoder, total_flops_backprob, total_flops_send, total_flops_recieve,total_flops_rest = 0,0,0,0,0,0,0,0
    for data in data_send_per_epoch_total:
        data_transfer_per_epoch += data
    for data in data_recieved_per_epoch_total:
        data_transfer_per_epoch += data
    for data in batches_abort_rate_total:
        average_dismissal_rate += data
    for flop in flops_client_forward_total:
        total_flops_forward += flop
    for flop in flops_client_encoder_total:
        total_flops_encoder += flop
    for flop in flops_client_backprop_total:
        total_flops_backprob += flop
    for flop in flops_client_send_total:
        total_flops_send += flop
    for flop in flops_client_recieve_total:
        total_flops_recieve += flop
    for flop in flops_client_rest_total:
        total_flops_rest += flop
    total_flops_model = total_flops_backprob + total_flops_encoder + total_flops_forward
    total_flops_all = total_flops_model+total_flops_send+total_flops_recieve+total_flops_rest
    print("total FLOPs forward: ", total_flops_forward)
    print("total FLOPs encoder: ", total_flops_encoder)
    print("total FLOPs backprob: ", total_flops_backprob)
    print("total FLOPs Model: ", total_flops_model)
    print("total FLOPs: ", total_flops_all)
    print("Average data transfer/epoch: ", data_transfer_per_epoch / epoch / 1000000, " MB")
    print("Average dismissal rate: ", average_dismissal_rate / epoch)

    wandb.config.update({"Average data transfer/epoch (MB): ": data_transfer_per_epoch / epoch / 1000000,
                         "Average dismissal rate: ": average_dismissal_rate / epoch})#,
                         #"total_MegaFLOPS_forward": total_flops_forward/1000000, "total_MegaFLOPS_encoder": total_flops_encoder/1000000,
                         #"total_MegaFLOPS_backprob": total_flops_backprob/1000000,"total_MegaFLOPS modal": total_flops_model/1000000 ,"total_MegaFLOPS": total_flops_all/1000000})

    msg = 0
    send_msg(s, 3, msg)


def initialize_model(s, msg):
    """
    if new connected client is not the first connected client,
    the initial weights are fetched from the server
    :param conn:
    """
    #msg = recieve_msg(s)
    if msg == 0:
        #print("msg == 0")
        pass
    else:
        print("msg != 0")
        client.load_state_dict(msg, strict=False)
        print("model successfully initialized")
    #print("start_training")
    # start_training(s)
    #train_epoch(s)


def main():
    """
    initialize device, client model, optimizer, loss and decoder and starts the training process
    """
    print_json()
    if count_flops:
        # Starts internal FLOPs counter | If there is an Error: See "from pypapi import events"
        high.start_counters([events.PAPI_FP_OPS,])

    global flops_client_forward_total, flops_client_encoder_total, flops_client_backprop_total, flops_client_send_total, flops_client_recieve_total, flops_client_rest_total
    flops_client_forward_total, flops_client_encoder_total, flops_client_backprop_total, flops_client_send_total, flops_client_recieve_total, flops_client_rest_total = [], [], [], [], [], []

    global zero_x, one_x, two_x, three_x, four_x, five_x, six_x, seven_x, eight_x, nine_x
    global zero_y, one_y, two_y, three_y, four_y, five_y, six_y, seven_y, eight_y, nine_y
    zero_x, one_x, two_x, three_x, four_x, five_x, six_x, seven_x, eight_x, nine_x = [], [], [], [], [], [], [], [], [], []
    zero_y, one_y, two_y, three_y, four_y, five_y, six_y, seven_y, eight_y, nine_y = [], [], [], [], [], [], [], [], [], []

    """
    global X_train, X_val, y_val, y_train, y_test, X_test
    sampling_frequency = 100
    datafolder = ptb_path
    task = 'superdiagnostic'
    outputfolder = mlb_path

    # Load PTB-XL data
    data, raw_labels = utils.load_dataset(datafolder, sampling_frequency)
    # Preprocess label data
    labels = utils.compute_label_aggregations(raw_labels, datafolder, task)
    # Select relevant data and convert to one-hot
    data, labels, Y, _ = utils.select_data(data, labels, task, min_samples=0, outputfolder=outputfolder)
    input_shape = data[0].shape
    print(input_shape)

    # 1-9 for training
    X_train = data[labels.strat_fold < 10]
    y_train = Y[labels.strat_fold < 10]
    # 10 for validation
    X_val = data[labels.strat_fold == 10]
    y_val = Y[labels.strat_fold == 10]

    #X_test = data[labels.strat_fold == 10]
    #y_test = Y[labels.strat_fold == 10]

    num_classes = 5  # <=== number of classes in the finetuning dataset
    input_shape = [1000, 12]  # <=== shape of samples, [None, 12] in case of different lengths

    print(X_train.shape, y_train.shape, X_val.shape, y_val.shape)#, X_test.shape, y_test.shape)

    import pickle

    standard_scaler = pickle.load(open("PTB-XL/ptb-xl/standard_scaler.pkl", "rb"))

    X_train = utils.apply_standardizer(X_train, standard_scaler)
    X_val = utils.apply_standardizer(X_val, standard_scaler)
    #X_test = utils.apply_standardizer(X_test, standard_scaler)
    """

    init()

    if plots: #visualize data
        Plots.load_dataset()
        Plots.plotten()
        Plots.ecg_signals()

    global epoch
    epoch = 0
    global encoder_grad_server
    encoder_grad_server = 0

    global data_send_per_epoch_total
    data_send_per_epoch_total = []
    global data_recieved_per_epoch_total
    data_recieved_per_epoch_total = []
    global batches_abort_rate_total
    batches_abort_rate_total = []

    global device
    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
    if (torch.cuda.is_available()):
        print("training on gpu")
    print("training on,", device)
    seed = 0
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    torch.backends.cudnn.benchmark = False
    torch.backends.cudnn.deterministic = True

    global client
    client = ClientMNIST()
    print("Start Client")
    #client.double().to(device)
    client.to(device)

    global optimizer
    #optimizer = SGD(client.parameters(), lr=lr, momentum=0.9)
    optimizer = SGD(client.parameters(), lr=0.002, momentum=0.5)
    #optimizer = AdamW(client.parameters(), lr=lr)
    print("Start Optimizer")

    global error
    error = nn.CrossEntropyLoss()
    #error = nn.BCELoss()
    print("Start loss calcu")

    global data_send_per_epoch
    global data_recieved_per_epoch
    data_send_per_epoch = 0
    data_recieved_per_epoch = 0

    #global scaler
    #scaler = MinMaxScaler()

    if autoencoder:
        global encode
        encode = EncodeMNIST()
        print("Start Encoder")
        if autoencoder_train == 0:
            encode.load_state_dict(torch.load("./convencoder_MNIST2.pth"))  # CPU
            print("Encoder model loaded")
        encode.eval()
        print("Start eval")
        encode.to(device)

        global optimizerencode
        optimizerencode = Adam(encode.parameters(), lr=lr)  ###

    if grad_encode:
        global grad_decoder
        grad_decoder = DecodeGradMNIST2()
        grad_decoder.load_state_dict(torch.load("./grad_decoder_MNIST3.pth"))
        grad_decoder.to(device)
        print("Grad decoder model loaded")

        global optimizer_grad_decoder
        optimizer_grad_decoder = Adam(grad_decoder.parameters(), lr=0.001)

    global error_grad_autoencoder
    error_grad_autoencoder = nn.MSELoss()

    #flops_client, params = thop.profile(client, inputs=(torch.rand(batchsize, 1, 28,
    #                                                              28).to(device),))
    #print(flops_client)
    #print(params)


    s = socket.socket()
    print("Start socket connect")
    s.connect((host, port))
    print("Socket connect success, to.", host, port)
    #initialize_model(s)

    serverHandler(s)



if __name__ == '__main__':
    main()
